<?php

namespace Drupal\mailgroup_imap;

use Ddeboer\Imap\ConnectionInterface;
use Ddeboer\Imap\Exception\InvalidResourceException;
use Ddeboer\Imap\MailboxInterface;
use Ddeboer\Imap\MessageInterface;
use Ddeboer\Imap\MessageIteratorInterface;
use Ddeboer\Imap\Search\Email\To;
use Ddeboer\Imap\Search\Flag\Unseen;
use Ddeboer\Imap\SearchExpression;
use Ddeboer\Imap\Server;
use Drupal\mailgroup\Entity\MailGroup;
use Drupal\mailgroup\Entity\MailGroupInterface;
use Drupal\mailgroup\Entity\MailGroupMessageInterface;
use Drupal\mailgroup\MessageParserFactory;
use Psr\Log\LoggerInterface;

/**
 * Default implementation of ImapHelperInterface.
 */
class ImapHelper implements ImapHelperInterface {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Cache of Connections.
   *
   * @var \Ddeboer\Imap\ConnectionInterface[]
   */
  protected $connections;

  /**
   * Cache of Mailboxes.
   *
   * @var \Ddeboer\Imap\MailboxInterface[]
   */
  protected $mailboxes;

  /**
   * The message parser factory.
   *
   * @var \Drupal\mailgroup\MessageParserInterface
   */
  protected $messageParserFactory;

  /**
   * Constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger interface.
   * @param \Drupal\mailgroup\MessageParserFactory $message_parser_factory
   *   The message parser factory.
   */
  public function __construct(LoggerInterface $logger, MessageParserFactory $message_parser_factory) {
    $this->logger = $logger;
    $this->messageParserFactory = $message_parser_factory;
  }

  /**
   * Destructor.
   */
  public function __destruct() {
    if (isset($this->connections)) {
      foreach ($this->connections as $connection) {
        $connection->close();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function connect(array $config): ConnectionInterface {

    $flags = '/imap';

    if ($config['secure']) {
      $flags .= '/ssl';
    }

    $server = new Server($config['server'], $config['port'], $flags);
    return $server->authenticate($config['username'], $config['password']);
  }

  /**
   * {@inheritdoc}
   */
  public function getConnection(MailGroupInterface $mail_group): ConnectionInterface {

    // If we have the connection cached, check if still open. If not, remove it
    // from our cache.
    if (isset($this->connections[$mail_group->id()])) {

      try {
        $this->connections[$mail_group->id()]->ping();
      }
      catch (InvalidResourceException $exception) {
        unset($this->connections[$mail_group->id()]);
      }
    }

    if (!isset($this->connections[$mail_group->id()])) {
      $this->connections[$mail_group->id()] = $this->connect($mail_group->getConnectionConfig());
    }

    return $this->connections[$mail_group->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function createMailGroupMessage(MailGroupInterface $mail_group, MessageInterface $message): ?MailGroupMessageInterface {

    $entity = NULL;

    try {
      $message_parser = $this->messageParserFactory->createFromString($message->getRawMessage(), $mail_group);

      /** @var \Drupal\mailgroup\Entity\MailGroupMessageInterface $entity */
      $entity = $message_parser->createMessageEntity();
      if ($entity) {
        $entity->save();
      }
    }
    catch (\Exception $exception) {
      $this->logger->error($exception->getMessage());
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getMailbox(MailGroupInterface $mail_group, string $mailbox_name = 'INBOX'): ?MailboxInterface {

    $mail_group_id = $mail_group->id();

    // If we have it cached, reuse it.
    if (isset($this->mailboxes[$mail_group_id])) {
      return $this->mailboxes[$mail_group_id];
    }

    $connection = $this->getConnection($mail_group);
    if ($connection->hasMailbox($mailbox_name)) {
      $this->mailboxes[$mail_group_id] = $connection->getMailbox($mailbox_name);
    }

    return $this->mailboxes[$mail_group_id] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessages(MailboxInterface $mailbox, string $group_email = NULL, bool $unread = TRUE): MessageIteratorInterface {

    $search = new SearchExpression();
    if ($unread) {
      $search->addCondition(new Unseen());
    }

    if (is_string($group_email)) {
      $search->addCondition(new To($group_email));
    }

    return $mailbox->getMessages($search);
  }

  /**
   * {@inheritdoc}
   */
  public function getMessagesForMailGroup(MailGroupInterface $mail_group, bool $unread = TRUE): array {

    $messages = [];
    $mailbox = $this->getMailbox($mail_group);

    if ($mailbox instanceof MailboxInterface) {

      $config = $mail_group->getConnectionConfig();
      $group_email = $config['filter_by_email'] ? $mail_group->getEmail() : NULL;

      $new_messages = $this->getMessages($mailbox, $group_email);
      /** @var \Ddeboer\Imap\MessageInterface $message */
      foreach ($new_messages as $message) {

        try {
          $mail_group->isAllowedToSend($message->getFrom()->getAddress());
          $messages[$message->getNumber()] = $message;
        }
        catch (\Exception $exception) {
          // Nothing for now. In time, we might want to allow logging these?
        }
      }
    }

    return $messages;
  }

  /**
   * {@inheritdoc}
   */
  public function listMailboxes(MailGroupInterface $mail_group): array {

    $connection = $this->getConnection($mail_group);
    $mailboxes = $connection->getMailboxes();

    $list = [];
    foreach ($mailboxes as $mailbox) {
      $list[] = $mailbox->getName();
    }

    return $list;
  }

  /**
   * {@inheritdoc}
   */
  public function processUnreadMessagesForAllMailGroups(): void {

    /** @var \Drupal\mailgroup\Entity\Storage\MailGroupStorageInterface $mailgroup_storage */
    $mailgroup_storage = \Drupal::entityTypeManager()->getStorage('mailgroup');

    /** @var \Drupal\mailgroup\Entity\MailGroupInterface[] $groups */
    $mailgroup_ids = $mailgroup_storage->getIdsByConnectionPlugin('imap');
    if (!empty($mailgroup_ids)) {

      $mailgroups = MailGroup::loadMultiple($mailgroup_ids);
      foreach ($mailgroups as $group) {

        try {
          $this->processUnreadMessagesForMailGroup($group);
        }
        catch (\Exception $exception) {
          $this->logger->error($exception->getMessage());
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processUnreadMessagesForMailGroup(MailGroupInterface $mail_group): array {

    $mail_group_messages = [];

    $messages = $this->getMessagesForMailGroup($mail_group);
    if (!empty($messages)) {

      $mailbox = $this->getMailbox($mail_group);
      foreach ($messages as $number => $message) {

        $mail_group_message = $this->createMailGroupMessage($mail_group, $message);
        if ($mail_group_message instanceof MailGroupMessageInterface) {
          $mail_group_messages[] = $mail_group_message;

          // Flag the message as read, so we don't import it again later.
          $mailbox->setFlag('\\Seen', "$number");
        }
      }
    }

    return $mail_group_messages;
  }

}

<?php

namespace Drupal\mailgroup_imap\Commands;

use Drupal\mailgroup_imap\ImapHelperInterface;
use Drush\Commands\DrushCommands;

/**
 * Actions using Drush.
 */
class ProcessAllGroups extends DrushCommands {

  /**
   * The IMAP helper.
   *
   * @var \Drupal\mailgroup_imap\ImapHelperInterface
   */
  protected $imapHelper;

  /**
   * Constructor.
   *
   * @param \Drupal\mailgroup_imap\ImapHelperInterface $imap_helper
   *   The IMAP helper.
   */
  public function __construct(ImapHelperInterface $imap_helper) {
    parent::__construct();
    $this->imapHelper = $imap_helper;
  }

  /**
   * Import all unread messages for all active Mail Groups.
   *
   * @command mailgroup-imap:process-all-groups
   * @aliases mailgroup-imap:pag
   */
  public function processAllGroups(): void {

    try {
      $this->imapHelper->processUnreadMessagesForAllMailGroups();
    }
    catch (\Exception $exception) {
      $this->io()->error($exception->getMessage());
    }
  }

}

<?php

namespace Drupal\mailgroup_imap;

/**
 * Shared getters for Mail plugin derivatives.
 */
trait MailgroupImapTrait {

  /**
   * {@inheritdoc}
   */
  public function getConnectionPluginIds() {
    return [
      'imap',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getModule() {
    return 'mailgroup_imap';
  }

}

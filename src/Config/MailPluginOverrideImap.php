<?php

namespace Drupal\mailgroup_imap\Config;

use Drupal\mailgroup\Config\MailPluginOverrideBase;
use Drupal\mailgroup_imap\MailgroupImapTrait;

/**
 * Adds Mail plugin derivative IDs for Mail Groups using IMAP.
 */
class MailPluginOverrideImap extends MailPluginOverrideBase {

  use MailgroupImapTrait;

}

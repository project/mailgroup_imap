<?php

namespace Drupal\mailgroup_imap\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\mailgroup\Entity\MailGroupInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Adds IMAP routes to MailGroup entities.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new RouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {

      if ($entity_type->entityClassImplements(MailGroupInterface::class)) {

        // Add the IMAP route for all entities. The access handler will check
        // if it is actually applicable on individual entities.
        $path = $entity_type->getLinkTemplate('canonical');
        $route = new Route("$path/imap");
        $route->addDefaults([
          '_form' => '\Drupal\mailgroup_imap\Form\ImapForm',
          '_title_callback' => '\Drupal\mailgroup_imap\Form\ImapForm::title',
        ])
          ->addRequirements([
            '_custom_access' => '\Drupal\mailgroup_imap\Access\MailGroupImapAccessCheck::checkAccess',
          ])
          ->setOption('_admin_route', TRUE)
          ->setOption('parameters', [
            $entity_type_id => ['type' => 'entity:' . $entity_type_id],
          ]);

        $collection->add("entity.$entity_type_id.imap", $route);
      }
    }
  }

}

<?php

namespace Drupal\mailgroup_imap\Form;

use Ddeboer\Imap\Exception\AuthenticationFailedException;
use Ddeboer\Imap\Exception\ResourceCheckFailureException;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\mailgroup_imap\ImapHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Helper form for current Mail Group IMAP plugin.
 */
class ImapForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'mailgroup_imap_helper';
  }

  /**
   * The IMAP helper.
   *
   * @var \Drupal\mailgroup_imap\ImapHelperInterface
   */
  protected $imapHelper;

  /**
   * The current Mail Group.
   *
   * @var \Drupal\mailgroup\Entity\MailGroupInterface
   */
  protected $mailgroup;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\mailgroup_imap\ImapHelperInterface $imap_helper
   *   The IMAP helper.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(RouteMatchInterface $route_match, ImapHelperInterface $imap_helper, MessengerInterface $messenger) {
    $this->imapHelper = $imap_helper;
    $this->mailgroup = $route_match->getParameter('mailgroup');
    $this->messenger = $messenger;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ImapForm {
    return new static(
      $container->get('current_route_match'),
      $container->get('mailgroup_imap.imap_helper'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form = [];

    try {

      $messages = $this->imapHelper->getMessagesForMailGroup($this->mailgroup);
      $count = count($messages);
      if ($count) {

        $form['info_messages'] = [
          '#markup' => $this->formatPlural($count, '1 message to import.', '@count messages to import.'),
        ];

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Import messages'),
          '#button_type' => 'primary',
        ];
      }
      else {
        $form['info_messages'] = [
          '#markup' => $this->t('There are no messages to import.'),
        ];
      }
    }
    catch (ResourceCheckFailureException $exception) {

      $message = $this->t('IMAP connection error: @error', ['@error' => $exception->getMessage()]);
      $this->messenger->addError($message);
    }
    catch (AuthenticationFailedException $exception) {

      $params = [
        ':url' => $this->mailgroup->toUrl('edit-form')->toString(),
      ];
      $message = $this->t('Check the plugin\'s <a href=":url" target="_blank" title=">connection credentials">connection credentials</a>', $params);
      $this->messenger->addError($message);
    }

    // By default, render the form using system-config-form.html.twig.
    $form['#theme'] = 'system_config_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $messages = $this->imapHelper->processUnreadMessagesForMailGroup($this->mailgroup);
    $this->messenger->addStatus($this->formatPlural(count($messages), '1 message created.', '@count messages created.'));
  }

  /**
   * Returns title for the form page.
   *
   * @return string
   *   The title.
   */
  public function title(): string {
    return "{$this->mailgroup->label()} IMAP";
  }

}

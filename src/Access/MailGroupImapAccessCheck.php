<?php

namespace Drupal\mailgroup_imap\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\mailgroup\Entity\MailGroupInterface;
use Drupal\mailgroup_imap\Plugin\Mailgroup\Connection\Imap;

/**
 * Access checks for Mail Group Imap.
 */
class MailGroupImapAccessCheck implements AccessInterface {

  /**
   * Check if the current user has access to the IMAP helper form.
   *
   * Access may only be granted if:
   * - We have a MailGroup entity from the route.
   * - The MailGroup is active.
   * - The connection plugin is our IMAP plugin.
   * - The user has permission to view the MailGroup.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The Access Result.
   */
  public function checkAccess(RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {

    $mailgroup = $route_match->getParameter('mailgroup');
    if (!$mailgroup instanceof MailGroupInterface) {
      return AccessResult::forbidden();
    }

    if ($mailgroup->isActive() && $mailgroup->getConnectionPlugin() instanceof Imap) {
      return $mailgroup->access('view', $account, TRUE);
    }

    return AccessResult::forbidden()->addCacheableDependency($mailgroup);
  }

}

<?php

namespace Drupal\mailgroup_imap\Plugin\Derivative;

use Drupal\mailgroup\Plugin\Derivative\MailgroupMailBase;
use Drupal\mailgroup_imap\MailgroupImapTrait;

/**
 * Provides deriver for IMAP Mail plugins.
 */
class MailgroupMailImap extends MailgroupMailBase {

  use MailgroupImapTrait;

}

<?php

namespace Drupal\mailgroup_imap\Plugin\Mail;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Mail\MailInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\mailgroup\Entity\MailGroup;
use Drupal\mailgroup\Entity\MailGroupInterface;
use Drupal\mailgroup_imap\ImapHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mailer\Mailer as SymfonyMailer;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mime\Email;

/**
 * Provides the Mail Group IMAP plugin to send emails.
 *
 * @Mail(
 *   id = "mailgroup_imap_imap",
 *   label = @Translation("Mail Group IMAP Mail"),
 *   description = @Translation("Mail Group IMAP Mail plugin."),
 *   deriver = "Drupal\mailgroup_imap\Plugin\Derivative\MailgroupMailImap",
 * )
 */
class MailgroupImap extends PluginBase implements ContainerFactoryPluginInterface, MailInterface {

  /**
   * The email validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The IMAP helper.
   *
   * @var \Drupal\mailgroup_imap\ImapHelperInterface
   */
  protected $imapHelper;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The Mail Group entity.
   *
   * @var \Drupal\mailgroup\Entity\MailGroupInterface
   */
  protected $mailgroup;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator service.
   * @param \Drupal\mailgroup_imap\ImapHelperInterface $imap_helper
   *   The IMAP helper.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EmailValidatorInterface $email_validator,
    ImapHelperInterface $imap_helper,
    LoggerChannelInterface $logger
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->emailValidator = $email_validator;
    $this->imapHelper = $imap_helper;
    $this->logger = $logger;

    // Find the corresponding Mail Group.
    $this->mailgroup = NULL;
    $mailgroup_id = $this->getDerivativeId();
    if ($mailgroup_id) {

      $mailgroup = MailGroup::load($mailgroup_id);
      if ($mailgroup instanceof MailGroupInterface
        && $mailgroup->getConnectionPluginId() === 'imap'
      ) {
        $this->mailgroup = $mailgroup;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return (new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('email.validator'),
      $container->get('mailgroup_imap.imap_helper'),
      $container->get('logger.channel.mailgroup_imap')
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function format(array $message) {
    // @todo Do we need to do something here?
    return $message;
  }

  /**
   * {@inheritdoc}
   */
  public function mail(array $message) {

    // Base error context.
    $context = [
      'id' => $this->pluginId,
      'to' => $message['to'],
      'subject' => $message['params']['subject'],
    ];

    if ($this->mailgroup === NULL) {

      $this->logger->error('Missing Mail Group for derivative Mail plugin with ID {id}. Mail to {to} with subject "{subject}" could not be sent.', $context);
      return FALSE;
    }
    else {

      $result = FALSE;
      $config = $this->mailgroup->getConnectionConfig();
      $smtp = $config['smtp'];

      // Build the SMTP DSN.
      // @see https://symfony.com/doc/current/mailer.html#using-built-in-transports
      // @see https://symfony.com/doc/current/mailer.html#tls-peer-verification
      $transport_dsn = "smtp://{$config['username']}:{$config['password']}@{$smtp['server']}:{$smtp['port']}?verify_peer={$smtp['secure']}";
      $transport = Transport::fromDsn($transport_dsn);

      try {

        $from = $message['params']['from'];
        $reply_to = $message['reply-to'];

        // Basically use all the setters in \Symfony\Component\Mime\Email that
        // seem relevant, or we have data for.
        $symfony_email = new Email();
        $symfony_email->subject($message['params']['subject']);
        $symfony_email->returnPath($reply_to);
        $symfony_email->sender($from);
        $symfony_email->from($from);
        $symfony_email->replyTo($reply_to);

        // Don't trip over 'undisclosed-recipients'.
        if ($this->emailValidator->isValid($message['to'])) {
          $symfony_email->to($message['to']);
        }

        // @todo \Drupal\mailgroup\MailHandler::sendMessage should not implode
        // bcc addresses, but do it in mailgroup_mail(). Then, we don't have
        // to reverse it here.
        $bcc_addresses = explode(', ', $message['params']['bcc']);
        foreach ($bcc_addresses as $bcc_address) {
          $symfony_email->addBcc($bcc_address);
        }

        $symfony_email->text($message['params']['message']);
        $symfony_email->html(Html::escape($message['params']['message']));

        $mailer = new SymfonyMailer($transport);
        $mailer->send($symfony_email);
        $result = TRUE;

        // On success, add to mailbox using IMAP, if possible.
        // @todo Figure this out.
      }
      catch (\Exception $exception) {

        $context['message'] = $exception->getMessage();
        $this->logger->error('Error sending email using Mail plugin with ID {id} to {to} with subject "{subject}": {message}', $context);
      }

      return $result;
    }
  }

}

<?php

namespace Drupal\mailgroup_imap\Plugin\Mailgroup\Connection;

use Drupal\Core\Form\FormStateInterface;
use Drupal\mailgroup\ConnectionBase;
use Drupal\mailgroup_imap\ImapHelperInterface;

/**
 * IMAP connection plugin.
 *
 * @Connection(
 *   id = "imap",
 *   label = @Translation("IMAP"),
 *   fields_to_encrypt = {
 *    "password" = "password"
 *   }
 * )
 */
class Imap extends ConnectionBase {

  /**
   * The IMAP helper.
   *
   * @var \Drupal\mailgroup_imap\ImapHelperInterface
   */
  protected $imapHelper;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->imapHelper = static::getImapHelper();
  }

  /**
   * {@inheritdoc}
   */
  public function getFields(FormStateInterface $form_state): array {

    // Disable form caching, or our AJAX callback will fail.
    $form_state->disableCache();

    $fields['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('The username of the email account.'),
      '#default_value' => $this->configuration['username'] ?? NULL,
      '#required' => TRUE,
    ];

    $fields['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('The password of the email account.'),
      '#default_value' => $this->configuration['password'] ?? NULL,
      '#required' => empty($this->configuration['password']),
    ];

    $fields['server'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server'),
      '#description' => $this->t('The IMAP server. For example: imap.example.com'),
      '#default_value' => $this->configuration['server'] ?? NULL,
      '#required' => TRUE,
    ];

    $fields['port'] = [
      '#type' => 'number',
      '#title' => $this->t('Port'),
      '#description' => $this->t('The IMAP port.'),
      '#default_value' => $this->configuration['port'] ?? 993,
    ];

    $fields['secure'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use SSL/TLS'),
      '#description' => $this->t('Use secure connection.'),
      '#default_value' => $this->configuration['secure'] ?? TRUE,
    ];

    $fields['folders'] = [
      '#type' => 'container',
      '#open' => TRUE,
      '#attributes' => ['id' => 'imap-folders'],
      '#tree' => TRUE,
    ];

    $fields['folders']['folder_refresh'] = [
      '#type' => 'button',
      '#value' => $this->t('Update IMAP folders'),
      '#limit_validation_errors' => [['connection_config']],
      '#submit' => [],
      '#executes_submit_callback' => FALSE,
      '#ajax' => [
        'callback' => [$this, 'updateImapFolders'],
        'wrapper' => 'imap-folders',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Fetching IMAP folders...'),
        ],
      ],
    ];

    $folders = static::getImapFolderOptions($this->configuration, $form_state);
    $fields['folders']['folder_read'] = [
      '#type' => 'select',
      '#title' => $this->t('Read folder'),
      '#description' => $this->t('The IMAP folder to check for new emails.'),
      '#options' => $folders['options'],
      '#default_value' => $folders['default'],
      '#disabled' => !(count($folders) > 1),
    ];

    $fields['filter_by_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Filter by the group email address'),
      '#description' => $this->t('Only process emails sent to the group email address. Useful when the email address is an alias of a mailbox that also serves other purposes.'),
      '#default_value' => $this->configuration['filter_by_email'] ?? NULL,
    ];

    // Check if we can support SMTP.
    $missing = $this->checkMissingSmtpDependencies();

    // SMTP support possible, show extra options.
    if (empty($missing)) {

      $fields['smtp_enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Use the SMTP server of the email account'),
        '#description' => $this->t('Use the SMTP server of the email account to send messages to the group members. If not enabled, the default mailer of the website will be used.'),
        '#default_value' => $this->configuration['smtp_enabled'] ?? NULL,
      ];

      $fields['smtp'] = [
        '#type' => 'details',
        '#title' => $this->t('SMTP'),
        '#description' => $this->t('The same username and password as for IMAP will be used.'),
        '#open' => TRUE,
        '#states' => [
          'visible' => [
            'input[name="connection_config[smtp_enabled]"]' => [
              'checked' => TRUE,
            ],
          ],
        ],
      ];

      $fields['smtp']['server'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Server'),
        '#description' => $this->t('The SMTP server. For example: smtp.example.com'),
        '#default_value' => $this->configuration['smtp']['server'] ?? NULL,
      ];

      $fields['smtp']['port'] = [
        '#type' => 'number',
        '#title' => $this->t('Port'),
        '#description' => $this->t('The SMTP port.'),
        '#default_value' => $this->configuration['smtp']['port'] ?? 465,
      ];

      $fields['smtp']['secure'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Use SSL/TLS'),
        '#description' => $this->t('Use secure connection.'),
        '#default_value' => $this->configuration['smtp']['secure'] ?? TRUE,
      ];
    }

    // SMTP support not possible, show what is required to make it possible.
    else {

      $missing_packages = implode(', ', $missing);
      $fields['smtp'] = [
        '#type' => 'inline_template',
        '#template' => '<div class="messages messages--info">{{ info }}</div>',
        '#context' => [
          'info' => $this->formatPlural(
            count($missing),
            'Install the package %missing_packages to support outgoing mail using SMTP.',
            'Install the packages %missing_packages to support outgoing mail using SMTP.',
            [
              '%missing_packages' => $missing_packages,
            ],
          ),
        ],
      ];
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function testConnection(): bool {

    try {
      $connection = $this->imapHelper->connect($this->getConfig());
      $connection->close();
    }
    catch (\Exception $exception) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function useMailPlugin(): bool {

    if ($this->configuration['smtp_enabled'] ?? FALSE) {
      // Make sure we don't crash if the optional dependencies are removed
      // without updating the config accordingly.
      return empty($this->checkMissingSmtpDependencies());
    }

    return FALSE;
  }

  /**
   * AJAX callback to update IMAP folders to the Mail Group form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public static function updateImapFolders(array &$form, FormStateInterface $form_state): array {

    /** @var \Drupal\mailgroup\Entity\MailGroupInterface $group */
    $group = $form_state->getFormObject()->getEntity();
    $config = $group->getConnectionConfig();

    $folders = static::getImapFolderOptions($config, $form_state);
    $form['connection']['connection_config']['folders']['folder_read']['#options'] = $folders['options'];
    $form['connection']['connection_config']['folders']['folder_read']['#default'] = $folders['default'];

    if (count($folders) > 1) {
      unset($form['connection']['connection_config']['folders']['folder_read']['#disabled']);
    }

    return $form['connection']['connection_config']['folders'];
  }

  /**
   * Check for missing SMTP dependencies.
   *
   * @return array
   *   Package names of missing dependencies. Empty array implies all
   *   dependencies are met.
   */
  protected function checkMissingSmtpDependencies(): array {

    $dependencies = [
      'symfony/mailer' => 'Symfony\Component\Mailer\Mailer',
    ];
    $missing = [];
    foreach ($dependencies as $name => $class) {
      if (!class_exists($class)) {
        $missing[] = $name;
      }
    }

    return $missing;
  }

  /**
   * Get the remote IMAP folders.
   *
   * @param array $config
   *   The plugin config.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The
   */
  protected static function getImapFolderOptions(array $config, FormStateInterface $form_state): array {

    $new_values = $form_state->getValue('connection_config');
    if (!is_null($new_values)) {

      // Ignore empty password fields from the form state.
      if (empty($new_values['password'])) {
        unset($new_values['password']);
      }

      $config = array_replace_recursive($config, $new_values);
    }
    $hash = md5(json_encode($config));

    /** @var \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store */
    $temp_store = \Drupal::service('tempstore.private');
    $temp_store_collection = $temp_store->get($hash);
    $remote_folders = $temp_store_collection->get('imap_remote_folders');

    if (is_null($remote_folders)
      && !empty($config['server'])
      && !empty($config['username'])
      && !empty($config['password'])
    ) {

      $remote_folders = [];

      try {
        $imap_helper = static::getImapHelper();
        $connection = $imap_helper->connect($config);
        $mailboxes = $connection->getMailboxes();
        $connection->close();

        foreach ($mailboxes as $name => $mailbox) {
          $remote_folders[$name] = $name;
        }

        $temp_store_collection->set('imap_remote_folders', $remote_folders);
      }
      catch (\Exception $exception) {
        \Drupal::messenger()->addError($exception->getMessage());
      }
    }

    $default = NULL;
    if (isset($config['folder_read']) && !empty($config['folder_read'])) {

      $folder_read = $config['folder_read'];
      if (!isset($remote_folders[$folder_read])) {
        $remote_folders[$folder_read] = $folder_read;
      }

      $default = $folder_read;
    }

    return [
      'options' => $remote_folders,
      'default' => $default,
    ];
  }

  /**
   * Get the IMAP helper service.
   *
   * @return \Drupal\mailgroup_imap\ImapHelperInterface
   *   The IMAP helper service.
   */
  protected static function getImapHelper(): ImapHelperInterface {
    return \Drupal::service('mailgroup_imap.imap_helper');
  }

}

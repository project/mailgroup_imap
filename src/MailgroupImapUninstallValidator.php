<?php

namespace Drupal\mailgroup_imap;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Prevents mailgroup_imap uninstall if there are Mail groups using the plugin.
 */
class MailgroupImapUninstallValidator implements ModuleUninstallValidatorInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TranslationInterface $string_translation) {
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($module) {
    $reasons = [];

    if ($module === 'mailgroup_imap') {

      if ($this->hasMailgroupsWithPlugin('imap')) {
        $reasons[] = $this->t('To uninstall Mail Group IMAP, first delete or update all <em>Mail groups</em> using the IMAP plugin');
      }
    }

    return $reasons;
  }

  /**
   * Determines if there are any Mail group entities using given plugin.
   *
   * @param string $plugin_id
   *   The connection plugin ID.
   *
   * @return bool
   *   TRUE if there are entities using the plugin, FALSE otherwise.
   */
  protected function hasMailgroupsWithPlugin(string $plugin_id) {
    $nodes = $this->entityTypeManager->getStorage('mailgroup')->getQuery()
      ->condition('connection', $plugin_id)
      ->accessCheck(FALSE)
      ->range(0, 1)
      ->execute();
    return !empty($nodes);
  }

}

<?php

namespace Drupal\mailgroup_imap;

use Ddeboer\Imap\ConnectionInterface;
use Ddeboer\Imap\MailboxInterface;
use Ddeboer\Imap\MessageInterface;
use Ddeboer\Imap\MessageIteratorInterface;
use Drupal\mailgroup\Entity\MailGroupInterface;
use Drupal\mailgroup\Entity\MailGroupMessageInterface;

/**
 * Defines IMAP related functions to import mails into Mail Group Messages.
 */
interface ImapHelperInterface {

  /**
   * Get IMAP connection for given configuration.
   *
   * To be used for basic connection instantiation or testing. The connection
   * must be closed by the caller.
   * Use getConnection() to get a connection for a Mail Group.
   *
   * @param array $config
   *   The IMAP connection configuration.
   *
   * @return \Ddeboer\Imap\ConnectionInterface
   *   The IMAP Connection.
   *
   * @throws \Ddeboer\Imap\Exception\AuthenticationFailedException
   * @throws \Ddeboer\Imap\Exception\ResourceCheckFailureException
   */
  public function connect(array $config): ConnectionInterface;

  /**
   * Get IMAP connection for given Mail Group.
   *
   * Must cache connections per Mail Group to prevent duplicate connections.
   * Cached connections will be closed on destruction of the service.
   *
   * @param \Drupal\mailgroup\Entity\MailGroupInterface $mail_group
   *   The Mail Group.
   *
   * @return \Ddeboer\Imap\ConnectionInterface
   *   The IMAP Connection.
   *
   * @throws \Ddeboer\Imap\Exception\AuthenticationFailedException
   * @throws \Ddeboer\Imap\Exception\ResourceCheckFailureException
   */
  public function getConnection(MailGroupInterface $mail_group): ConnectionInterface;

  /**
   * Create Mail Group Message from (IMAP) Message.
   *
   * @param \Drupal\mailgroup\Entity\MailGroupInterface $mail_group
   *   The Mail Group.
   * @param \Ddeboer\Imap\MessageInterface $message
   *   The message.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMessageInterface|null
   *   The Mail Group Message.
   */
  public function createMailGroupMessage(MailGroupInterface $mail_group, MessageInterface $message): ?MailGroupMessageInterface;

  /**
   * Get Mailbox.
   *
   * @param \Drupal\mailgroup\Entity\MailGroupInterface $mail_group
   *   The Mail Group.
   * @param string $mailbox_name
   *   The Mailbox name. Defaults to 'INBOX'.
   *
   * @return \Ddeboer\Imap\MailboxInterface|null
   *   The Mailbox.
   */
  public function getMailbox(MailGroupInterface $mail_group, string $mailbox_name = 'INBOX'): ?MailboxInterface;

  /**
   * Get messages in Mailbox.
   *
   * @param \Ddeboer\Imap\MailboxInterface $mailbox
   *   The Mailbox.
   * @param string|null $group_email
   *   The email address to filter the messages on. Defaults to NULL, no
   *   filtering.
   * @param bool $unread
   *   Filter for unread messages. Defaults to TRUE.
   *
   * @return \Ddeboer\Imap\MessageIteratorInterface
   *   The messages.
   */
  public function getMessages(MailboxInterface $mailbox, string $group_email = NULL, bool $unread = TRUE): MessageIteratorInterface;

  /**
   * Get messages for Mail Group.
   *
   * This will already filter messages from senders that are not allowed.
   *
   * @param \Drupal\mailgroup\Entity\MailGroupInterface $mail_group
   *   The Mail Group.
   * @param bool $unread
   *   Unread messages only. Defaults to TRUE.
   *
   * @return array
   *   The (unread) messages, keyed by the message number in the Mailbox.
   *
   * @throws \Ddeboer\Imap\Exception\AuthenticationFailedException
   * @throws \Ddeboer\Imap\Exception\ResourceCheckFailureException
   */
  public function getMessagesForMailGroup(MailGroupInterface $mail_group, bool $unread = TRUE): array;

  /**
   * Get list of all available mailbox names in the remote.
   *
   * @param \Drupal\mailgroup\Entity\MailGroupInterface $mail_group
   *   The Mail Group.
   *
   * @return array
   *   List of mailbox, aka folder, names.
   */
  public function listMailboxes(MailGroupInterface $mail_group): array;

  /**
   * Import all unread messages for all active Mail Groups.
   */
  public function processUnreadMessagesForAllMailGroups(): void;

  /**
   * Process unread messages for Mail Group.
   *
   * @param \Drupal\mailgroup\Entity\MailGroupInterface $mail_group
   *   The Mail Group.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMessageInterface[]
   *   The created Mail Group Messages.
   *
   * @throws \Ddeboer\Imap\Exception\AuthenticationFailedException
   * @throws \Ddeboer\Imap\Exception\ResourceCheckFailureException
   */
  public function processUnreadMessagesForMailGroup(MailGroupInterface $mail_group): array;

}
